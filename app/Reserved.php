<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Room;

class Reserved extends Model
{
    public $timestamps = false;
    protected $fillable = ['start_time', 'end_time', 'room_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }
}
