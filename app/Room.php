<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['address_id', 'label', 'size', 'description', 'price', 'bedrooms', 'housing_id', 'owner_id' ];

    public function reservations()
    {
        return $this->hasMany(Reserved::class);
    }

    public function equipment()
    {
        return $this->belongsToMany(Equipment::class, 'equipment_rooms');
    }

    public function housing()
    {
        return $this->belongsTo(HousingType::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function addEquipment(array $equipments)
    {
        return $this->equipment()->attach($equipments);
    }

    public function removeAllEquipments()
    {
        return $this->equipment()->detach();
    }

    public function hasEquipment(int $id)
    {
        return $this->equipment->contains($id);
    }
}
