<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const Advertiser = 1;
    const Customer = 2;
}
