<?php

namespace App\Http\Controllers;

use App\User;
use App\Equipment;
use App\Room;
use App\Address;
use Auth;
use App\Image;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\HousingType;
use App\Equipments;

class RoomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
        $this->middleware('room.owner')->only(['update', 'edit', 'destroy']);
        $this->middleware('auth.advertiser')->only('create');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::all();
        return view('room.index', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $equipments = Equipment::all();
        $housings = HousingType::all();
        return view('room.create', compact('equipments', 'housings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->validate([
            'label' => 'required|max:50',
            'description' => 'required',
            'country' => 'required',
            'city' => 'required',
            'bedrooms' => 'required|integer',
            'size' => 'required|integer',
            'price' => 'required|between:0,999.999.99',
            'housing_id' => 'required|integer',
            'equipments' => ''
        ]);
        $address = Address::create($fields);

        $fields['owner_id'] = Auth::user()->id;
        $fields['address_id'] = $address->id;

        $room = Room::create($fields);
        $room->addEquipment($fields['equipments']);

        return redirect(route('rooms.show', $room));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        $owner = User::find($room->owner_id);
        return view('room.show', compact('room', 'owner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        $equipments = Equipment::all();
        $housings = HousingType::all();
        return view('room.edit', compact('room', 'equipments', 'housings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        $fields = $request->validate([
            'label' => 'required|max:50',
            'description' => 'required',
            'bedrooms' => 'required|integer',
            'size' => 'required|integer',
            'price' => 'required|between:0,999.999.99',
            'housing_id' => 'required|integer',
            'equipments' => ''
        ]);

        $room->update($fields);
        if(!empty($request->equipments)){
            $room->removeAllEquipments();
            $room->addEquipment($fields['equipments']);
        }
        return redirect(route('rooms.show', $room));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        $room->delete();
        return redirect(route('rooms.index'));
    }
}
