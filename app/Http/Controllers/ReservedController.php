<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reserved;
use App\Room;
use Auth;
use App\Role;

class ReservedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('auth.advertiser')->except('store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Room $room)
    {
        if(Auth::user()->role_id == Role::Advertiser){
            $rooms = Auth::user()->rooms;
        }else $rooms = Auth::user()->reserveds;

        return view('reserved.index', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Room $room, Request $request)
    {
        $fields = $request->validate([
            'start_time' => 'required',
            'end_time' => 'required',
        ]);
        
        if(Reserved::where([['start_time', '=', $request->start_time], ['end_time', '=', $request->end_time]])->first()){
            return redirect(route('rooms.show', $room));
        }

        $fields['room_id'] = $room->id;
        $fields['user_id'] = Auth::user()->id;

        Reserved::create($fields);

        return redirect(route('rooms.show', $room));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Reserved $reserveds)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
