<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;

class CheckAuthAdvertiser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->role_id == (Role::Advertiser))
            return $next($request);
        return redirect(route('rooms.index'));
    }
}
