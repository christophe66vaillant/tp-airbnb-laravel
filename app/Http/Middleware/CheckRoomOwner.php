<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckRoomOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->room->owner_id == Auth::user()->id)
            return $next($request);
        return redirect(route('rooms.show', $request->room));

    }
}
