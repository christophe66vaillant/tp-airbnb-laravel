<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rooms', function (Blueprint $table){
            $table->foreign('housing_id')
                ->references('id')
                ->on('housing_types')
                ->onDelete('cascade');
            $table->foreign('address_id')
                ->references('id')
                ->on('addresses')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rooms', function (Blueprint $table){
            $table->dropForeign(['housing_id']);
            $table->dropForeign(['address_id']);
            $table->dropIndex('rooms_housing_id_foreign');
            $table->dropIndex('rooms_address_id_foreign');
        });
    }
}
