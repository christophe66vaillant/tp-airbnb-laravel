<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->unsignedInteger('size');
            $table->unsignedInteger('bedrooms');
            $table->text('description');
            $table->float('price');
            $table->unsignedInteger('housing_id');
            $table->unsignedInteger('address_id');
            $table->unsignedInteger('owner_id');
            $table->timestamps();
        });

        Schema::table('rooms', function (Blueprint $table){
            $table->foreign('owner_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
