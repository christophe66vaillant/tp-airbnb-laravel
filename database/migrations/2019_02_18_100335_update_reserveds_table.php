<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReservedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reserveds', function (Blueprint $table){
            $table->foreign('room_id')
                ->references('id')
                ->on('rooms')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unique(['room_id', 'user_id', 'start_time', 'end_time']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reserveds', function (Blueprint $table){
            $table->dropForeign(['room_id']);
            $table->dropForeign(['user_id']);
            $table->dropIndex('reserveds_user_id_foreign');
            $table->dropUnique(['room_id', 'user_id', 'start_time', 'end_time']);
        });
    }
}
