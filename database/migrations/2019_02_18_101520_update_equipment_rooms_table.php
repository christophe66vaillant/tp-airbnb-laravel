<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEquipmentRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipment_rooms', function (Blueprint $table){
            $table->foreign('room_id')
                ->references('id')
                ->on('rooms')
                ->onDelete('cascade');
            $table->foreign('equipment_id')
                ->references('id')
                ->on('equipment')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipment_rooms', function (Blueprint $table){
            $table->dropForeign(['room_id']);
            $table->dropForeign(['equipment_id']);
            $table->dropIndex('equipment_room_room_id_foreign');
            $table->dropIndex('equipment_room_equipment_id_foreign');
        });
    }
}
