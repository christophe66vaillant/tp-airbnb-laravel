<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::insert([
            'label' => 'Advertiser'
        ]);

        \App\Role::insert([
            'label' => 'Customer'
        ]);
        factory(App\User::class, 20)->create();
        factory(App\Address::class, 50)->create();
        factory(App\HousingType::class, 50)->create();
        factory(App\Equipment::class, 50)->create();
        factory(App\Room::class, 50)->create();
        factory(App\EquipmentRoom::class, 150)->create();
        factory(App\Reserved::class, 200)->create();
    }
}
