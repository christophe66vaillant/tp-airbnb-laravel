<?php

use Faker\Generator as Faker;

$factory->define(App\HousingType::class, function (Faker $faker) {
    return [
        'label' => $faker->word,
    ];
});
