<?php

use Faker\Generator as Faker;

$factory->define(App\EquipmentRoom::class, function (Faker $faker) {
    return [
        'equipment_id' => App\Equipment::all()->random()->id,
        'room_id' => App\Room::all()->random()->id
    ];
});
