<?php

use Faker\Generator as Faker;

$factory->define(App\Reserved::class, function (Faker $faker) {
    return [
        'room_id' => App\Room::all()->random()->id,
        'user_id' => App\User::where('role_id', '=', App\Role::Customer)->get()->random()->id,
        'start_time' => $faker->dateTime(),
        'end_time' => $faker->dateTime('+50 years'),
    ];
});
