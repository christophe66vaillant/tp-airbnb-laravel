<?php

use Faker\Generator as Faker;

$factory->define(App\Room::class, function (Faker $faker) {
    return [
        'label' => $faker->realText($faker->numberBetween(10,40)),
        'size' => $faker->numberBetween(100, 3000),
        'bedrooms' => $faker->numberBetween(1, 20),
        'description' =>  $faker->realText($faker->numberBetween(350,1000)),
        'price' => $faker->randomFloat(2, $min = 0, $max = 50000),
        'housing_id' => App\HousingType::all()->random()->id,
        'address_id' => App\Address::all()->random()->id,
        'owner_id' => App\User::all()->random()->id
    ];
});
