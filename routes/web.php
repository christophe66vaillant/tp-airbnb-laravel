<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/rooms', 'HomeController@index')->name('home');

Route::resource('rooms', 'RoomController')->parameters(['room' => '[0-9]+']);

Route::resource('/rooms/{room}/reserveds', 'ReservedController')->parameters(['reserveds' => '[0-9]+'])->only(['index', 'show', 'store', 'destroy']);
