@extends('layouts.app')

@section('title', 'Reservations')

@section('content')
    @if( isset($rooms[0]->reservations[0]) )
        @if(Auth::user()->role_id == App\Role::Advertiser)
            <h2 class="text-center"><b>My Reserved Rooms : </b></h2>
        @else
            <h2 class="text-center"><b>My Reservations : </b></h2>
        @endif
    <hr>
        @foreach($rooms as $room)
            @foreach($room->reservations as $reservation)
                <div>
                    <b>Room name : </b><p>{{$room->label}}</p>
                    <b>By : </b><p>{{$reservation->user->name}}</p>
                    <b>Start : </b><p>{{$reservation->start_time}}</p>
                    <b>To : </b><p>{{$reservation->end_time}}</p>
                    <hr>
                </div>
            @endforeach
        @endforeach
    @else
        <h2 class="text-center"><b>No rooms available.</b></h2>
    @endif
@endsection
