@extends('layouts.app')

@section('title', $room->label)

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    </div>
@endif

    <div>
        <b>Room name : </b><p>{{str_replace('.', '', $room->label)}}, by {{$owner->name}}</p>
        <b>Little description : </b><p>{{$room->description}}</p>
        <b>Price : </b><p>{{$room->price}}€ / nuit</p>
        <b>Location : </b><p>{{$room->address->country}}, {{$room->address->city}}</p>
        <b>Housing type : </b><p>{{$room->housing->label}}, {{$room->bedrooms}} @if( $room->bedrooms > 1) lits @else lit @endif ({{$room->size}}m²)</p>
        @if(isset($room->equipment[0])) <b>Equipements : </b>@foreach($room->equipment as $equip)<p>{{$equip->label }}</p>@endforeach @else <b><p>No equipments available.</p></b>@endif
        @if(Auth::user())
            @if(Auth::user()->role_id == App\Role::Customer)
                <form action="{{route('reserveds.store', $room)}}" method="post">
                    @csrf
                    <div class="text-center"><p>Please choose a date : </p><input type="datetime-local"  name="start_time" value="<?php echo date('Y-m-d\TH:i') ?>"><input type="datetime-local" value="<?php echo date('Y-m-d\TH:i', strtotime( '+1 week' )) ?>" name="end_time"></div>
                    <p class="text-center"><b><button type="submit" class="btn btn-outline-primary w-100">Reserve now !</button></b></p>
                </form>
            @endif
        @else
            <h2 class="text-center"><b>Please <i><a href="{{route('register')}}">register</a></i> or <i><a href="{{route('login')}}">login</a></i> here to reserve this room.</b></h2>
        @endif
        @if( !empty(Auth::user()) && (Auth::user() && Auth::user()->id == $room->owner_id))
            <h2 class="text-center"><b>Click <i><a href="{{route('rooms.edit', $room)}}">here</a></i> to edit this room.</b></h2>
            <h2 class="text-center"><b>Click <button type="button" data-toggle="modal" data-target="#infos" class="btn btn-primary"><i>here</i></button> to delete this room.</b></h2>
            <div class="modal" id="infos">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Are your sure ?</h4>
                        </div>
                        <div class="modal-body">
                            If you click on "Delete", the room "{{$room->label}}" will be deleted.
                        </div>
                        <div class="modal-footer">
                            <form action="{{route('rooms.destroy', $room)}}" method="post">
                            @csrf
                            @method('DELETE')
                                <button class="btn btn-danger" type="submit"><em>Delete</em></button>
                            </form>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><em>Close</em></button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <hr>
    </div>
@endsection
