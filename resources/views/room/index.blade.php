@extends('layouts.app')

@section('title', 'Home')

@section('content')
    @if( isset($rooms[0]) )
        @foreach($rooms as $room)
            <div>
                <b>Room name : </b><p>{{$room->label}}</p>
                <b>Little description : </b><p>{{str_limit($room->description, 250, '...')}}<a href="/rooms/{{$room->id}}">See rooms details.</a></p>
                <b>Price : </b><p>{{$room->price}}€ / nuit</p>
                <hr>
            </div>
        @endforeach
    @else
        <h2 class="text-center"><b>No rooms available.</b></h2>
    @endif
@endsection
