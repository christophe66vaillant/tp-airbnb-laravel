@extends('layouts.app')

@section('title', 'Create room')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    </div>
@endif

<form action="{{route('rooms.store')}}" method="POST" enctype="multipart/form-data">
@csrf
    <h1> Create Room : </h1>
        <label>
            <span>Country : </span>
            <input type="text" name="country" value="{{old('country')}}">
        </label>
        <label>
            <span>City : </span>
            <input type="text" name="city" value="{{old('city')}}">
        </label>
        <label>
            <span>Name (Little description) : </span>
            <input type="text" name="label" value="{{old('label')}}">
        </label>
        <label>
            <span>Description : </span>
            <textarea type="text" name="description" placeholder="Discribe your room, insert link or other.">{{old('description')}}</textarea>
        </label>
        <label>
            <span>Number of bed : </span>
            <input type="text" name="bedrooms" placeholder="Number of bed." value="{{old('bedrooms')}}">
        </label>
        <label>
            <span>Size of your room : </span>
            <input type="text" name="size" placeholder="Size." value="{{old('size')}}">
        </label>
        <label>
            <span>Price : </span>
            <input type="text" name="price" placeholder="Price, for ONE night." value="{{old('price')}}">
        </label>
        <div class="select-checkbox">
            @foreach ($equipments as $equipment)
                <label>
                    <span>{{$equipment->label}}</span>
                    <input type="checkbox" name="equipments[]" value="{{$equipment->id}}">
                </label>
            @endforeach
        </div>
        <label>
            <select name="housing_id">
                <option value="0">Please, choose a house type : </option>
                @foreach ($housings as $housing)
                    <option value="{{$housing->id}}">{{$housing->label}}</option><br>
                @endforeach
            </select>
        </label>
        <label>
            <label>Choose your pictures (0-5) : </label>
            <input name="images[]" type="file" multiple>
        </label>
        <button type="submit" class="btn btn-outline-primary">Create now !</button>
    </form>
@endsection