@extends('layouts.app')

@section('title', 'Update room')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    </div>
@endif

<form action="{{route('rooms.update', $room)}}" method="POST" enctype="multipart/form-data">
@csrf
    <input type="hidden" name="_method" value="PUT">
    <h1> Update Room : </h1>
        <label>
            <span>Name (Little description) : </span>
            <input type="text" name="label" value="{{old('label') ?: $room->label}}">
        </label>
        <label>
            <span>Description : </span>
            <textarea type="text" name="description" placeholder="Discribe your room, insert link or other.">{{old('description') ?: $room->description}}</textarea>
        </label>
        <label>
            <span>Number of bed : </span>
            <input type="text" name="bedrooms" placeholder="Number of bed." value="{{old('bedrooms') ?: $room->bedrooms}}">
        </label>
        <label>
            <span>Size of your room : </span>
            <input type="text" name="size" placeholder="Size." value="{{old('size') ?: $room->size}}">
        </label>
        <label>
            <span>Price : </span>
            <input type="text" name="price" placeholder="Price, for ONE night." value="{{old('price') ?: $room->price}}">
        </label>
        <div class="select-checkbox">
            @foreach ($equipments as $equipment)
                <label>
                    <span>{{$equipment->label}}</span>
                    <input
                        type="checkbox"
                        value="{{$equipment->id}}"
                        name="equipments[]"
                        {{$room->hasEquipment($equipment->id) ? 'checked': ''}}
                    >
                </label>
            @endforeach
        </div>
        <label>
            <select name="housing_id">
                <option value="0">Please, choose a house type : </option>
                @foreach ($housings as $housing)
                    <option value="{{$housing->id}}" {{($room->housing->id == $housing->id) ? 'selected' : ''}}>{{$housing->label}}</option><br>
                @endforeach
            </select>
        </label>
        <button type="submit" class="btn btn-outline-primary">Update now !</button>
    </form>
@endsection